<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;
class UserController extends Controller
{
    public static function login_API($username,$password){
	  	$url = 'http://api.telkomakses.co.id/API/sso/auth_sso_post.php';
		$data = 'username=' . $username . '&password=' . $password;	
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec( $ch );
		$hasil = json_decode($response, true);	
		return $hasil;	
	}
	public function login(Request $req){
		$messages = [
            'username.required' => 'username kosong',
            'password.required' => 'password kosong'
        ];
        $this->validate($req,[
            'username' => 'required',
            'password' => 'required'
        ], $messages);
		$result = self::login_API($req->username,$req->password);
        if ($result['auth'] == "Yes"){
        	$result = (object) $result;
        	$result->nik = $req->username;
            Session::put('auth', $result);
            return redirect('/');
        }else{
        	switch ($result['auth']) {
	            // Valid User, Wrong Password
	            case 'WP':
	                $alertText = '<strong>Password</strong> salah. User akan dikunci jika 3x salah password';
	                break;
	            // User not exists
	            case 'No':
	                $alertText = '<strong>NIK</strong> tidak terdaftar';
	                break;
	            default:
	                $alertText = '<strong>login gagal gan!</strong>';
	                break;
	        }
	        //dd($result);
        	return redirect()->back()->with('alerts', [
				['type' => 'error', 'text' => $alertText
			]]);
        }
	}
	public function logout(){
        Session::forget('auth');
        return redirect('/login');
    }
}
